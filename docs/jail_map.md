# jail map
A jail map is a map you design without any regular way to enter or leave. It can thus only be entered or left with the help of a [chat command](chat_commands) like [@warp](chat_commands/warp), [@goto](chat_commands/goto) or [@recall](chat_commands/recall).

Such a map can have various purposes:
 * an environment for testing on a production server without any interference by regular players
 * "parking lot" for your own bots
 * private meeting place for server personnel
 * temporary confinement of characters suspected of breaking server rules, especially illicit bots.
 * an alternative to [@ban](chat_commands/ban) as a way of suspending abusive players
