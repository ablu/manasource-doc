# requests for comments
#  Pages needing reviews and comments

##  This section is for collecting concepts for technical improvements

__Bertram__
 * [ Playerset layers handling improvement proposal](playerset_handling_improvement)

__Crush__
 * [Pixel-accurate route finding](pixel-accurate_route_finding)
 * [Runtime map modification](runtime_map_modification)
 * [permissions.xml](rights.xml.md)
 * [global_events.lua](global_events.lua.md)
 * [character transfer interface](character_transfer_interface)
 * [persistent variables for LUA script engine](persistent_variables_for_lua_script_engine)
 * [Visual manaserv configuration interface](visual_manaserv_configuration_interface)
 * [Server-sided special handling concept](server-sided_special_handling_concept)

__Jaxad0127__
 * [Client actor hierarchy](client_actor_hierarchy)

##  These pages need to be reviewed, completed and fixed in order to officially include them in the manasource wiki documentation

__ExceptionFault__
 * [Manaweb Connectors](manaweb_connectors)
